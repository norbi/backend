class MyCustomParser < Faraday::Response::Middleware
  def on_complete(env)
    json = MultiJson.load(env[:body], symbolize_keys: true)
    env[:body] = {
      data: json[:sports],
      errors: json[:errors],
      metadata: json[:metadata]
    }
  end
end

Her::API.setup url: "https://www.betvictor.com/" do |c|
  # Request
  c.use Faraday::Request::UrlEncoded

  # Response
  # c.use Her::Middleware::DefaultParseJSON
  c.use MyCustomParser

  # Cache
  c.use :http_cache, store: Rails.cache, serializer: Marshal

  # Adapter
  c.adapter Faraday::Adapter::NetHttp
end
