# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  namespace :api do
    namespace :v1 do
      resources :sports, only: :index
      resources :events, only: :index
      resources :outcomes, only: :index
    end
  end
end
