# frozen_string_literal: true

class Api::V1::OutcomesController < ApplicationController
  def index
    render json: collection.to_json
  end

  private

  def collection
    @collection ||= Event
                      .find_by_sport_and_id(params[:sport_id], params[:event_id])
                      .outcomes.flatten
  end
end
