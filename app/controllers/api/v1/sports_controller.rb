# frozen_string_literal: true

class Api::V1::SportsController < ApplicationController
  def index
    render json: collection.to_json
  end

  private

  def collection
    @collection ||= Sport
                      .all
                      .sort_by! { |element| element.pos }
                      .map(&:json_attributes)
  end
end
