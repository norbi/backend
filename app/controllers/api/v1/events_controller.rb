# frozen_string_literal: true

class Api::V1::EventsController < ApplicationController
  def index
    render json: collection.to_json
  end

  private

  def collection
    @collection ||= Sport
                      .find(params[:sport_id])
                      .events
                      .sort_by { |element| element.pos }
                      .map(&:json_attributes)

  end
end
