# frozen_string_literal: true

class Competition
  include Her::Model
  has_many :events, class_name: 'Event'

end
