# frozen_string_literal: true

class Event
  include Her::Model
  # include ActiveModel::Model

  def json_attributes
    attributes.slice('id', 'desc', 'pos', 'oppADesc', 'oppBDesc', 'sport_id')
  end

  def outcomes
    @outcomes ||= markets.map { |market| market.fetch('o') }
  end

  def self.find_by_sport_and_id(sport_id, event_id)
    Sport.find(sport_id).events.find { |element| element.id.to_s == event_id.to_s }
  end
end
