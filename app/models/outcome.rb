# frozen_string_literal: true

class Outcome
  include Her::Model

  def json_attributes
    attributes.slice('oid', 'd', 'po', 'keyDimension')
  end
end
