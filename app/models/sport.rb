# frozen_string_literal: true

class Sport
  include Her::Model
  collection_path "bv_in_play/v2/en-gb/1/mini_inplay.json"

  has_many :comp, class_name: 'Competition'

  def json_attributes
    attributes.slice('id', 'desc', 'pos')
  end

  def events
    @events ||= comp.map(&:events).flatten
  end

  def self.find(identifier)
    all.to_a.find { |element| element.id.to_s == identifier.to_s }
  end

end
