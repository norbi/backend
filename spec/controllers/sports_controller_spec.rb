require 'rails_helper'

RSpec.describe Api::V1::SportsController do
  describe "GET index" do
    
    it "index should answer with status 200" do
      controller.stub(:collection).and_return([])
      get :index
      expect(response).to have_http_status(:ok)
    end

    it "index should response a json collection" do
      controller.stub(:collection).and_return([{id:1, desc: 'asd', pos: 1},{id: 2, desc: 'asdasd', pos: 2}])
      get :index

      response_collection = JSON.parse response.body
      expect(response_collection.size).to eq(2)
      expect(response_collection.last).to have_key('id')
      expect(response_collection.last).to have_key('desc')
      expect(response_collection.last).to have_key('pos')
    end
  end
end